# Desafio Desenvolvedor App

## Instruções
* Clone este repositório.
* Use '$ npm i' para instalar as dependências necessárias.
* Para rodar em debug use '$ npm run android', com um dispositívo conectado ao computador.
* Para rodar em release use '$ npm run android --variant=release / npm run ios --variant=release', com um dispositívo conectado ao computador.

##### Stack
* React Native, com CI/CD configurado no Microsoft AppCenter

##### Distrubuição
* O app pode ser testado em release através do link: https://install.appcenter.ms/users/julio.cesar-clicksoft.com.br/apps/appanimale/distribution_groups/distribute


###### Dúvidas: gjceewh@gmail.com
