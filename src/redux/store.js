import {createStore, applyMiddleware} from 'redux'
import {persistStore, persistReducer} from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'
import ReduxPromise from 'redux-promise'
import reducers from './reducers'

const persistConfig = {
  key: '@Personalpass',
  storage: AsyncStorage,
  blacklist: ['events', 'createEvent'],
}

const middlewares = [ReduxPromise]

const persistedReducer = persistReducer(persistConfig, reducers)
const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore)
const store = createStoreWithMiddleware(persistedReducer)
let persistor = persistStore(store)

export {store, persistor}
