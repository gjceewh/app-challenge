import { combineReducers } from 'redux'
import initial from './initial'
import auth from './auth'
// import professionals from './professionals'
// import appointments from './appointments'
// Import redux here

export default combineReducers({
  initial,
  auth,
//   professionals,
//   appointments,
  // Insert redux here
})
