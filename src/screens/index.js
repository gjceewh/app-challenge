import Catalog from './Catalog'
import Home from './Home'
import Menu from './Menu'
import Product from './Product'

export  { 
	Catalog,
    Home,
    Menu,
    Product
}