import React, { Component } from 'react';
import { View, Button, Text, ScrollView, Modal, TextInput, Image, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import Ionicon from 'react-native-vector-icons/dist/Ionicons'


import {
    Product
} from './components'

import { connect } from 'react-redux'
import { Creators as authActions } from '../../redux/reducers/auth'

import API, { AxiosWithHeaders } from '../../services/api'

const customData = require('../../services/products.json');




class Catalog extends Component {
    constructor(props) {
        super(props);


        this.state = {
        };

    }

    componentDidMount() {
    }


    render() {
        const {title} = this.props.route.params
        return (
            <>

            <ScrollView contentContainerStyle={{  flexGrow: 1 }}>



            <View style={{backgroundColor: '#fff',  }}>
                <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                padding: 20,
                            }}>

                            <Text style={{ flex: 9, fontFamily: 'Futura Md BT', fontSize: 14, color: '#000', textAlign: 'center' }}>{title.toUpperCase()}</Text>

                            <TouchableWithoutFeedback
                                onPress={() => { this.props.navigation.navigate('Catalog') }}
                            >
                                <Image
                                style={{position: 'absolute', flex: 1, height: 25, right: 0, marginHorizontal: 10,resizeMode: 'contain' }}
                                source={require('../../../assets/img/bag.png')}
                                />
                            </TouchableWithoutFeedback>

                            

                            {
                                customData.slice(22,27).map((item)=> {
                                    return <Product 
                                    title={item.productName} 
                                    image={item.items[0].images[0].imageUrl} 
                                    price={`R$ ${item.items[0].sellers[0].commertialOffer.Price}`}
                                    />
                                })
                            }


                        </View> 
                
            


            </View>


            </ScrollView>

            </>
            );
        }
    }


    const mapStateToProps = state => {
        return {
            user: state.auth.user,
            token: state.auth.token
        }
    }

    const Styles = {
        title: {
            borderWidth: 1,
            borderColor: '#fff',
            color: '#000',
            fontFamily: 'Futura Md BT', 
            fontSize: 13,
            marginHorizontal: 20,
            marginTop: 40,
            marginBottom: 20    
        },
        subtitle: {

            color: '#626262',
            fontFamily: 'Futura Bk BT', 
            fontSize: 13,
            marginHorizontal: 20,
            marginTop: 40,
            marginBottom: 20    
        }
    }

    // export default connect(mapStateToProps, { ...authActions })(HomeScreen)
    export default Catalog

