import React, { Component } from 'react';
import { View, Button, Text, ScrollView, Modal, TextInput, Image, TouchableWithoutFeedback, TouchableNativeFeedback, ImageBackground } from 'react-native';
import Ionicon from 'react-native-vector-icons/dist/Ionicons'


import {
    AnimaleOro,
    Banner,
    Category,
    InsideAnimale,
    SearchBox
} from './components'

import { connect } from 'react-redux'
import { Creators as authActions } from '../../redux/reducers/auth'
import API, { AxiosWithHeaders } from '../../services/api'




class Menu extends Component {
    constructor(props) {
        super(props);


        this.state = {
        };

    }


    render() {
        return (
            <>
                 
                <ScrollView contentContainerStyle={{  flexGrow: 1 }}>

                    <View style={{backgroundColor: '#fff',  }}>
                        

                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                padding: 20,
                            }}>

                            <Text style={{ flex: 9, fontFamily: 'Futura Md BT', fontSize: 14, color: '#000', textAlign: 'center' }}>MENU</Text>

                            <TouchableWithoutFeedback
                                onPress={() => { this.props.navigation.navigate('Catalog') }}
                            >
                                <Image
                                style={{position: 'absolute', flex: 1, height: 25, right: 0, marginHorizontal: 10,resizeMode: 'contain' }}
                                source={require('../../../assets/img/bag.png')}
                                />
                            </TouchableWithoutFeedback>


                        </View> 

                        <SearchBox value={''} />


                            

                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <View style={{flexDirection: 'row', justifyContent: 'flex-start',   }}>
                                    
                                    <Category 
                                        title={'Malha'} 
                                        image={require('../../../assets/img/malha.png')}
                                        onPress={() => { this.props.navigation.navigate('Catalog', { title: 'Malha', subtitle: 'asdksad'}) }}
                                        />
                                    <Category title={'Jaquetas'} image={require('../../../assets/img/jaquetas.png')}/>
                                    <Category title={'Tricot'} image={require('../../../assets/img/tricot.png')}/>
                                    <Category title={'Blusas'} image={require('../../../assets/img/blusas.png')}/>
                                    <Category title={'Shop'} image={require('../../../assets/img/shop.png')}/>

                                    
                                </View>
                            </ScrollView>

                            
                            <Banner title={'COLEÇÃO'} image={require('../../../assets/img/colecao.png')} />
                            <Banner title={'NOVIDADES'} image={require('../../../assets/img/novidades.png')} />
                            <Banner title={'ACESSÓRIOS'} image={require('../../../assets/img/acessorios.png')} />
                            <Banner title={'INTIMATES'} image={require('../../../assets/img/intimates.png')} />
                            <Banner title={'SALE'} image={require('../../../assets/img/sale.png')} />
                            <AnimaleOro />
                            <InsideAnimale />




                            <View style={{height: 20}}></View>
                            

                    </View>
                    

                </ScrollView>

            </>
        );
    }
}


const mapStateToProps = state => {
    return {
        user: state.auth.user,
        token: state.auth.token
    }
}

// export default connect(mapStateToProps, { ...authActions })(HomeScreen)
export default Menu