import React, { Component } from 'react';
import { View, Button, Text, ScrollView, Modal, TextInput, Image, TouchableWithoutFeedback, PixelRatio, ImageBackground } from 'react-native';
import Ionicon from 'react-native-vector-icons/dist/Ionicons'
import Icon from 'react-native-vector-icons/dist/FontAwesome'




const Banner = ({title, image}) => {
	return <>

		<View style={{flexDirection: 'row',
			justifyContent: 'flex-end',
			alignItems: 'center', 
			marginHorizontal: 7,
			height: 104,
			marginVertical: 5,
			borderRadius: 2,
			backgroundColor: 'rgba(193,195,194,1)',
			}}> 
			<Text style={{flex: 6, fontFamily: 'Futura Md BT', fontSize: 14, color: '#fff', textAlign:  'left', marginLeft: 30 }}>{title}</Text>

				<Image
					style={{flex: 4, height: 104, resizeMode: 'contain' }}
					source={image} 
					/>
		</View>
	</>
}

const AnimaleOro = ({}) => {
	return <>
		<ImageBackground 
            source={require('../../../assets/img/animale-oro.png')} 
            style={{
                marginVertical: 5,
                marginHorizontal: 7,
                borderRadius: 2,
                flex: 1,
                resizeMode: "cover",
                justifyContent: "center",

                }}
            >
            <View style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
                paddingHorizontal: 30,
                width: '100%',
                height: 116,
                borderRadius: 2,
                }}>
                <Text style={{
                    fontFamily: 'Futura Md BT', 
                    fontSize: 14, 
                    color: '#fff', 
                    textAlign:  'left', 
                    marginLeft: 0 
                }}>ANIMALE ORO</Text>
            </View>
        </ImageBackground>
	</>
}

const InsideAnimale = ({}) => {
	return <>
		<ImageBackground 
            source={require('../../../assets/img/inside-animale.png')} 
            style={{
                marginVertical: 5,
                marginHorizontal: 7,
                borderRadius: 2,
                flex: 1,
                resizeMode: "cover",
                justifyContent: "center",

                }}
            >
            <View style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
                paddingHorizontal: 30,
                width: '100%',
                height: 200,
                borderRadius: 2,
                }}>
                
            </View>
        </ImageBackground>
	</>
}


const Category = ({title, image, onPress}) => {
	return <>
		
	<TouchableWithoutFeedback
	onPress={onPress}
	>
	<View style={{
		flexDirection: 'column', 
		alignItems: 'center', 
		padding: 16}}>

		<Image
		style={{width: 64, height: 64,  }}
		source={image}
		/>
		<Text style={{fontFamily: 'Futura Bk BT', fontSize: 13, padding: 3}}>{title}</Text>
		</View>
		</TouchableWithoutFeedback>
	</>
}

const SearchBox = ({value}) => {
	return <>
		<View style={{
			flexDirection: 'row', 
			alignItems: 'center', 
			backgroundColor: '#F3F3F3',
			marginHorizontal: 16,
			marginVertical: 10,
			padding: 5,
			paddingLeft: 20,
			borderRadius: 2

			}}>
			

			<Icon
				
				name={'search'}
				size={15}
				color={'#2e2e2e'}	
			/>

			<TextInput
		        width={200}
		        textAlign={'left'}
		        placeholder={'Busque por produtos…'}
		        placeholderTextColor={'#969696'}
		        value={value}
		        style={{fontFamily: 'Futura Bk BT', fontSize: 14, marginLeft: 10}}
		      />
		</View>
	</>
}



export {
	AnimaleOro,
	Banner,
	Category,
	InsideAnimale,
	SearchBox
}