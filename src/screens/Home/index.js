import React, { Component } from 'react';
import { View, Button, Text, ScrollView, Modal, TextInput, Image, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import Ionicon from 'react-native-vector-icons/dist/Ionicons'


import {
    Options,
    Preview,
    Product,
    ShopNow,
    Trends
} from './components'

import { connect } from 'react-redux'
import { Creators as authActions } from '../../redux/reducers/auth'
import AsyncStorage from '@react-native-community/async-storage';

import API, { AxiosWithHeaders } from '../../services/api'

const customData = require('../../services/products.json');




class Home extends Component {


    constructor(props) {
        super(props);


        this.state = {
            viewedProducts: []
        };

    }

    async componentDidMount() {
        // console.log(customData[1].items[0].sellers[0].commertialOffer.Price)
        this.getViewedProducts({})
        

    }

    setViewedProduct = async (item) => {
        this.state.viewedProducts.push(item)

        try {
            await AsyncStorage.setItem('ViewedProducts', JSON.stringify(this.state.viewedProducts))
        } catch (e) {
            console.log(`cache error\n ${e}`)
        }   
    }

    clearViewedProducts = async () => {
        await this.setState({viewedProducts: []})
        await AsyncStorage.setItem('ViewedProducts',JSON.stringify([]))
    }

    getViewedProducts = async (item) => {
        let viewedProducts = await AsyncStorage.getItem('ViewedProducts')
        this.setState({viewedProducts: JSON.parse(viewedProducts)})
    }  


    render() {
        return (
            <>

            <ScrollView contentContainerStyle={{  flexGrow: 1 }}>

            <View style={{backgroundColor: '#fff',  }}>

            <ImageBackground 
            source={require('../../../assets/img/new-wave-preview.png')} 
            style={{

                borderRadius: 2,
                flex: 1,

                justifyContent: "center",

            }}
            >
            <View style={{
                flexDirection: 'column',
                justifyContent: 'flex-end',
                alignItems: 'center',
                height: 600,

            }}>
            <Text style={{fontFamily: 'Didot', color:'#fff', fontSize: 15, letterSpacing: 5}}>PREVIEW</Text>
            <Text style={{fontFamily: 'Didot', color:'#fff', fontSize: 30, letterSpacing: 5}}>NEW WAVE</Text>
            <ShopNow />

            </View>
            </ImageBackground>

            <View style={{flexDirection: 'row', justifyContent: 'space-between'   }}>
            <Text style={Styles.title}>NEW IN</Text>
            <Text style={Styles.subtitle}>Ver tudo</Text>
            </View>

            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row', justifyContent: 'flex-start', padding: 10  }}>

            <Trends title={'HOT TRENDS'} image={require('../../../assets/img/hot-trends.png')}/>



            <Trends title={'JAQUETAS'} image={require('../../../assets/img/hot-trends2.png')}/>


            </View>
            </ScrollView>

            <View style={{height: 50}}></View>

            <View style={{margin: 10}}>
            <Preview />
            </View>

            {
                this.state.viewedProducts.length ? <View style={{flexDirection: 'row', justifyContent: 'space-between'   }}>
                <Text style={Styles.title}>VISTOS RECENTEMENTE</Text>
                <TouchableWithoutFeedback onPress={this.clearViewedProducts}>
                <Text style={Styles.subtitle}>Limpar</Text>
                </TouchableWithoutFeedback>
                </View> : null
            }


            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row', justifyContent: 'flex-start',  }}>
            
            {
                this.state.viewedProducts.slice(0,15).map((item)=> {
                    return <Product 
                    title={item.productName} 
                    image={item.items[0].images[0].imageUrl} 
                    price={`R$ ${item.items[0].sellers[0].commertialOffer.Price}`}
                    onPress={()=>{
                        this.props.navigation.navigate('Product', item)
                        this.setViewedProduct(item)
                    }}
                    />
                })
            }

            </View>

            </ScrollView>

            <View style={{flexDirection: 'row', justifyContent: 'space-between'   }}>
            <Text style={Styles.title}>VOCÊ PODE CURTIR</Text>
            </View>

            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row', justifyContent: 'flex-start',  }}>
            {/*
                <Product image={require('../../../assets/img/product.png')}/>
                <Product image={require('../../../assets/img/product2.png')}/>
                <Text>{JSON.stringify(this.state.viewedProducts)}</Text>

            */}




            {
                customData.slice(10,15).map((item)=> {
                    return <Product 
                    title={item.productName} 
                    image={item.items[0].images[0].imageUrl} 
                    price={`R$ ${item.items[0].sellers[0].commertialOffer.Price}`}
                    onPress={()=>{
                        this.props.navigation.navigate('Product', item)
                        this.setViewedProduct(item)
                    }}
                    />
                })
            }
            </View>

            </ScrollView>

            <View
            style={{
                borderBottomColor: 'rgba(216, 216, 216, 0.3)',
                borderBottomWidth: 1,
            }}
            />

            <Options title={'Encontre uma loja'} icon={require('../../../assets/img/map.png')}/>
            <Options title={'Fale com um vendedor'} icon={require('../../../assets/img/chat.png')}/>


            </View>


            </ScrollView>

            </>
            );
        }
    }


    const mapStateToProps = state => {
        return {
            user: state.auth.user,
            token: state.auth.token
        }
    }

    const Styles = {
        title: {
            borderWidth: 1,
            borderColor: '#fff',
            color: '#000',
            fontFamily: 'Futura Md BT', 
            fontSize: 13,
            marginHorizontal: 20,
            marginTop: 40,
            marginBottom: 20    
        },
        subtitle: {

            color: '#626262',
            fontFamily: 'Futura Bk BT', 
            fontSize: 13,
            marginHorizontal: 20,
            marginTop: 40,
            marginBottom: 20    
        }
    }

    // export default connect(mapStateToProps, { ...authActions })(HomeScreen)
    export default Home

