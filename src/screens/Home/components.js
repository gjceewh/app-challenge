import React, { Component } from 'react';
import { View, Button, Text, ScrollView, Modal, TextInput, Image, TouchableWithoutFeedback, PixelRatio, ImageBackground } from 'react-native';
import Ionicon from 'react-native-vector-icons/dist/Ionicons'
import Icon from 'react-native-vector-icons/dist/FontAwesome'


const Product = ({title,image, price,onPress}) => {
	return <>
	<TouchableWithoutFeedback onPress={onPress}>
		<View style={{flexDirection: 'column'}}>
		<Image style={{
			height: 360,
			width: 270,
			resizeMode: 'contain'}}
			source={{uri: image}}/>
			<View style={{padding: 20}}>
			<Text style={{
				color: '#626262',
				fontFamily: 'Futura Bk BT', 
				fontSize: 13,
				width: 220,
				paddingVertical: 4
			}}>{title}</Text>
			<Text style={{fontFamily: 'Futura Bk BT', fontWeight: 'bold'}}>{price}</Text>
			<Text style={{
				color: '#626262',
				fontFamily: 'Futura Bk BT', 
				fontSize: 13,


			}}>5x de R$ 105,80</Text>
			</View>
			</View>
	</TouchableWithoutFeedback>
		</>
	}

	const Options = ({title, icon}) => {
		return <>
		<View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems:  'center',  padding: 30,borderBottomColor: 'rgba(216, 216, 216, 0.3)',
		borderBottomWidth: 1,  }}>
		<Image style={{width: 35, height: 35, resizeMode: 'contain'}} source={icon}/>
		<Text style={{flex: 3, fontFamily: 'Futura Bk BT', marginHorizontal: 20}}>{title}</Text>
		<Ionicon
		style={{
			fontWeight: '900',
			color: 'rgba(112,112,112,0.5)',
			transform: [{ rotate: '180deg' }],
		}}
		name={'ios-arrow-back'}
		size={21}
		color={'red'} />
		</View>
		</>
	}


	const Trends = ({title, image}) => {
		return <>
		<View style={{marginRight: 10}}>
		<ImageBackground 
		source={image} 
		style={{

			borderRadius: 2,
			flex: 1,
			resizeMode: 'contain',
			justifyContent: "center",

		}}
		>
		<View style={{ 
			flexDirection: 'column',
			justifyContent: 'center',
			alignItems: 'center',
			marginLeft: 15, 
			height: 300, 
			width: 230}}>

			<Text style={{
				fontFamily: 'Futura Md BT', 
				fontSize: 14,
				color: '#FAFAFA',
				letterSpacing: 3
			}}
			>{title}</Text>
			<ShopNow size={30} marginVertical={15}/>

			</View>
			</ImageBackground>
			</View>
			</>
		}

		const Preview = ({}) => {
			return <>
			<ImageBackground 
			source={require('../../../assets/img/preview.png')} 
			style={{

				borderRadius: 2,
				flex: 1,
				resizeMode: 'cover',
				justifyContent: "center",

			}}
			>
			<View style={{

				flexDirection: 'column',
				justifyContent: 'center',
				alignItems: 'center',
				marginLeft: 15, 
				height: 520, 

			}}>



			</View>
			</ImageBackground>
			</>
		}

		const ShopNow = ({size,marginVertical}) => {
			return <>
			<View>
			<Text style={{
				fontFamily: 'Futura Md BT', 
				fontSize: 12,
				marginVertical: marginVertical,
				paddingHorizontal: size,
				paddingVertical: size / 4,
				color: '#fff',
				borderWidth: 1,
				borderColor: '#fff'
			}}
			>SHOP NOW</Text>
			</View>
			</>
		}

		ShopNow.defaultProps = {
			size: 70,
			marginVertical: 40
		}

		export {
			Options,
			Preview,
			Product,
			ShopNow,
			Trends
		}