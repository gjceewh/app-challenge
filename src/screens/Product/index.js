import React, { Component } from 'react';
import { View, Button, Text, ScrollView, Modal, TextInput, Image, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import Ionicon from 'react-native-vector-icons/dist/Ionicons'
import Carousel from 'react-native-banner-carousel';
import FastImage from 'react-native-fast-image'

import {
    AddToBag,
    Option,
    Options,
    ProductItem
} from './components'

import { connect } from 'react-redux'
import { Creators as authActions } from '../../redux/reducers/auth'

import API, { AxiosWithHeaders } from '../../services/api'

const customData = require('../../services/products.json');




class Product extends Component {
    constructor(props) {
        super(props);


        this.state = {
        };

    }

    componentDidMount() {
    }


    render() {
        const {productName} = this.props.route.params
        const product = this.props.route.params
        return (
            <>

            <ScrollView contentContainerStyle={{  flexGrow: 1 }}>



            <View style={{backgroundColor: '#fff',  }}>
                {/*
                    <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                padding: 20,
                            }}>

                            <Text style={{ flex: 9, fontFamily: 'Futura Md BT', fontSize: 14, color: '#000', textAlign: 'center' }}>asd</Text>

                            <TouchableWithoutFeedback
                                onPress={() => { this.props.navigation.navigate('Catalog') }}
                            >
                                <Image
                                    style={{position: 'absolute', flex: 1, height: 25, right: 0, marginHorizontal: 10,resizeMode: 'contain' }}
                                    source={require('../../../assets/img/bag.png')}
                                />
                            </TouchableWithoutFeedback>



                        </View> 
                    */}


                    {

                        <Carousel autoplay={false}>


                        {
                        // product.items[0].images.slice(0,product.items[0].images.length - 1).map((item,index)=> {
                        //     return <ImageBackground 
                        //     source={{uri: item.imageUrl}} 
                        //     style={{

                        //         borderRadius: 2,
                        //         flex: 1,

                        //         justifyContent: "center",

                        //     }}
                        //     >
                        //     <View style={{
                        //         flexDirection: 'column',
                        //         justifyContent: 'flex-end',
                        //         alignItems: 'center',
                        //         height: 600,

                        //     }}>


                        //     </View>
                        //     </ImageBackground>
                        // })

                        // product.items[0].images.slice(0,product.items[0].images.length - 1).map((item,index)=> {
                            product.items[0].images.slice(0,1).map((item,index)=> {
                                return <FastImage
                                style={{height: 600 }}
                                source={{
                                    uri: item.imageUrl,
                                    priority: FastImage.priority.high,
                                }}
                                resizeMode={FastImage.resizeMode.contain}
                                />
                            })
                        }


                        </Carousel>

                    }

                    <Text style={{fontFamily: 'Futura Bk BT', textAlign: 'center', fontSize: 15, paddingTop: 30, padding: 10 }}>{product.productName}</Text>

                    <Text style={{fontFamily: 'Futura Bk BT', textAlign: 'center', fontWeight: 'bold', padding: 10}}>{`R$ ${product.items[0].sellers[0].commertialOffer.Price}`}</Text>
                    <Text style={{
                        color: '#626262',
                        fontFamily: 'Futura Bk BT',
                        textAlign: 'center',
                        fontSize: 13,
                        padding: 10


                    }}>5x de R$ 105,80</Text>

                    <AddToBag />


                    <View style={{margin: 20, padding: 30, backgroundColor: '#F3F3F3'}}>
                    <Text style={{fontFamily: 'Futura Bk BT', color: '#000',fontSize: 12, paddingTop: 20, paddingBottom: 30}}>REF: 59.12.0258_6012</Text>
                    <Text style={{fontFamily: 'Futura Bk BT', color: '#000',fontSize: 16}}>

                    A calça de malha texturizada vermelha tem shape justo, cintura e tornozelos finalizados por elástico, comprimento cropped e cinto estilo paraquedista. Combine com botas de cano curto e camisas de seda para um look chic, ou use com tênis e camisetas básicas para uma pegada urbana.
                    </Text>

                    <View style={{height: 50}}></View>

                    <Option title={'Informações sobre a peça'} />
                    <Option title={'Cuidados com o produto'} />
                    <Option title={'Devolução e troca fácil'} />

                    </View>


                    <View style={{flexDirection: 'column',  backgroundColor: '#F3F3F3', padding: 20 }}>
                        <Text style={{fontFamily: 'Futura Md BT', fontSize: 13, paddingVertical: 20}}>PRODUTOS RELACIONADOS</Text>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <View style={{flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#F3F3F3' }}>


                        {
                            customData.slice(55,60).map((item)=> {
                                return <ProductItem
                                title={item.productName} 
                                image={item.items[0].images[0].imageUrl} 
                                price={`R$ ${item.items[0].sellers[0].commertialOffer.Price}`}
                                onPress={()=>{
                                    this.props.navigation.navigate('Product', item)
                                }}
                                />
                            })
                        }
                        </View>
                        </ScrollView>
                    </View>





                    <Options title={'Encontre uma loja'} icon={require('../../../assets/img/map.png')}/>
                    <Options title={'Fale com um vendedor'} icon={require('../../../assets/img/chat.png')}/>



                    </View>


                    </ScrollView>

                    </>
                    );
}
}


const mapStateToProps = state => {
    return {
        user: state.auth.user,
        token: state.auth.token
    }
}

const Styles = {
    title: {
        borderWidth: 1,
        borderColor: '#fff',
        color: '#000',
        fontFamily: 'Futura Md BT', 
        fontSize: 13,
        marginHorizontal: 20,
        marginTop: 40,
        marginBottom: 20    
    },
    subtitle: {

        color: '#626262',
        fontFamily: 'Futura Bk BT', 
        fontSize: 13,
        marginHorizontal: 20,
        marginTop: 40,
        marginBottom: 20    
    }
}

                // export default connect(mapStateToProps, { ...authActions })(HomeScreen)
                export default Product

