import React, { Component } from 'react';
import { View, Button, Text, ScrollView, Modal, TextInput, Image, TouchableWithoutFeedback, PixelRatio, ImageBackground } from 'react-native';
import Ionicon from 'react-native-vector-icons/dist/Ionicons'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import FastImage from 'react-native-fast-image'

const AddToBag = ({}) => {
	return <>
	<View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end', paddingHorizontal: 30, paddingVertical: 15, margin: 20, backgroundColor: '#000'}}>
	<Image
	style={{  height: 25,  tintColor: '#fff', resizeMode: 'contain', paddingBottom: 7, }}
	source={require('../../../assets/img/bag.png')}
	/>
	<Text style={{fontFamily: 'Futura Bk BT', fontWeight: 'bold', fontSize: 13, color: '#fff'}}>ADICIONAR À SACOLA</Text>

	</View>
	</>
}

const Option = ({title}) => {
	return <>
	<View style={{
		flexDirection: 'column',
		justifyContent: 'center',
		paddingVertical: 25,
		borderBottomColor: 'rgba(216, 216, 216, 1)',
		borderBottomWidth: 1, 
	}}>
		<View style={{flexDirection: 'row', justifyContent: 'space-between'  }}>
			<Text style={{fontFamily: 'Futura Bk BT',  color: '#000',fontSize: 14,}}>{title}</Text>
			<Ionicon
				style={{
					fontWeight: '900',
					color: 'rgba(112,112,112,0.5)',
					transform: [{ rotate: '180deg' }],
				}}
				name={'ios-arrow-back'}
				size={21}
				color={'red'} />
		</View>
	</View>
	</>
}

const Options = ({title, icon}) => {
		return <>
		<View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems:  'center',  padding: 30,borderBottomColor: 'rgba(216, 216, 216, 0.3)',
		borderBottomWidth: 1,  }}>
		<Image style={{width: 35, height: 35, resizeMode: 'contain'}} source={icon}/>
		<Text style={{flex: 3, fontFamily: 'Futura Bk BT', marginHorizontal: 20}}>{title}</Text>
		<Ionicon
		style={{
			fontWeight: '900',
			color: 'rgba(112,112,112,0.5)',
			transform: [{ rotate: '180deg' }],
		}}
		name={'ios-arrow-back'}
		size={21}
		color={'red'} />
		</View>
		</>
	}

const ProductItem = ({title,image, price,onPress}) => {
	return <>
	<TouchableWithoutFeedback onPress={onPress}>
		<View style={{flexDirection: 'column', paddingRight: 20}}>
		<FastImage style={{
			height: 360,
			width: 270,
			resizeMode: 'contain'}}
			source={{uri: image, }}/>
			<View style={{padding: 20}}>
			<Text style={{
				color: '#626262',
				fontFamily: 'Futura Bk BT', 
				fontSize: 13,
				width: 220,
				paddingVertical: 4
			}}>{title}</Text>
			<Text style={{fontFamily: 'Futura Bk BT', fontWeight: 'bold'}}>{price}</Text>
			<Text style={{
				color: '#626262',
				fontFamily: 'Futura Bk BT', 
				fontSize: 13,


			}}>5x de R$ 105,80</Text>
			</View>
			</View>
	</TouchableWithoutFeedback>
		</>
	}

export  {
	AddToBag,
	Option,
	Options,
	ProductItem
}