// In App.js in a new project

import * as React from 'react';
import { View, Text, Icon, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


import * as views from '../screens'



const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


function TabRoutes ()  {
	return <>
	<Tab.Navigator
	screenOptions={({ route }) => ({
		tabBarIcon: ({ focused, color, size }) => {
			let iconName;

			switch (route.name) {
				case 'Home':
					return (
						<Image
						style={focused ? Styles.iconActive : Styles.iconInactive }
						source={require('../../assets/img/home.png')}
						/>              
						);
				case 'Menu':
					return (
						<Image
						style={focused ? Styles.iconActive : Styles.iconInactive }
						source={require('../../assets/img/menu.png')}
						/> 
						);
				case 'MyList':
					return (
						<Image
						style={focused ? Styles.iconActive : Styles.iconInactive }
						source={require('../../assets/img/my_list.png')}
						/> 
						);
				default:
					return (
						<Image
						style={focused ? Styles.iconActive : Styles.iconInactive }
						source={require('../../assets/img/account.png')}
						/> 
						);
			}
		},
	})}
	tabBarOptions={{
		activeTintColor: '#000000',
		inactiveTintColor: '#969696',
		labelStyle: {
			fontFamily: 'sofia_pro_regular',
			fontSize: 9,
			margin: 0,
			marginBottom: 6,
		},
		style: {
			height: 60,
			elevation: 2,
		},
	}}>

	<Tab.Screen
	name="Home"
	component={views.Home}
	options={{ tabBarLabel: 'inicio' }}
	/>

	<Tab.Screen
	name="Menu"
	component={views.Menu}
	options={{ tabBarLabel: 'menu' }}
	/>

	<Tab.Screen
	name="MyList"
	component={views.Home}
	options={{ tabBarLabel: 'my list' }}
	/>

	<Tab.Screen
	name="Account"
	component={views.Home}
	options={{ tabBarLabel: 'conta' }}
	/>

	</Tab.Navigator>
	</>;
};



function Routes() {
	return (
		<NavigationContainer>
		<Stack.Navigator>
		<Stack.Screen name="HomeScreen" component={TabRoutes} options={{ headerShown: false }} />
		<Stack.Screen name="Catalog" component={views.Catalog} options={{ headerShown: false }} />
		<Stack.Screen name="Product" component={views.Product} options={{ headerShown: false }} />
		</Stack.Navigator>
		</NavigationContainer>
		);
}

const Styles = {
	iconActive: { height: 25, resizeMode: 'contain', tintColor: "#000"  },
	iconInactive: { height: 25, resizeMode: 'contain', tintColor: "#969696"  }
}

export default Routes;